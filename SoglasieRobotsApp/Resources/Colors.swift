//
//  Colors.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 29.01.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}

struct Colors {
    static let orange = UIColor(rgb: 0xFF6602)
    static let red = UIColor(rgb: 0xFF0000)
    static let green = UIColor(rgb: 0x5D745E)
    static let black = UIColor(rgb: 0x2D2D3A)
}
