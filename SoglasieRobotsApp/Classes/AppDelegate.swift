//
//  AppDelegate.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 29.01.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    // MARK: - Services
    private let cryptoService = ServiceLayer.shared.cryptoService
    private let backgroundTimeService = ServiceLayer.shared.backgroundTimeService

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        if JailbreakChecker.isJailbreak() {
            fatalError("Jailbreak detected")
        }
        
        GMSServices.provideAPIKey("AIzaSyCagTxLDbYdiFRQJTGClh7hw7Y6PHM5agg")
        return true
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        let blankVC = UIViewController()
        blankVC.view.backgroundColor = .black
        self.window?.rootViewController?.present(blankVC, animated: false, completion: nil)
        self.backgroundTimeService.saveTimeInBackground()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        self.window?.rootViewController?.dismiss(animated: false) { [unowned self] in
            if self.backgroundTimeService.needToLogOut() {
                self.cryptoService.deleteAllSecrets()
                if let navigationController = self.window?.rootViewController as? UINavigationController {
                    navigationController.popToRootViewController(animated: true)
                }
            }
        }
    }
    
    
}

