//
//  TimeInBackgroundCalculator.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 22.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

final class TimeInBackgroundCalculatorService {
    
    private let secondsToLogout: Double = 120
    private var startTime: Date = Date()
    
    func saveTimeInBackground(){
        startTime = Date()
    }
    
    func needToLogOut() -> Bool {
        let secondsLeft = self.secondsToLogout - abs(startTime.timeIntervalSinceNow)
        if secondsLeft > 0 {
            return false
        } else {
            return true
        }
    }
}
