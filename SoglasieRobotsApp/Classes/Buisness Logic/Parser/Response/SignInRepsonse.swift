//
//  SignInRepsonse.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 30.01.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

struct SuccessResponse: Codable {
    
    let data: SignInResponse
    
}

struct SignInResponse: Codable {
    
    let user: User
    let accessToken: String
    
    private enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case user = "user"
    }
        
}
    

