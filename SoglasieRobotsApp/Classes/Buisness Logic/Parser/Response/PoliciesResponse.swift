//
//  PoliciesResponse.swift
//  SoglasieRobotsApp
//
//  Created by Alex on 13.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

struct PoliciesSuccessResponse: Codable {
    
    let data: PoliciesResponse
    
}

struct PoliciesResponse: Codable {
    
    let policyList: [Policy]
    
    private enum CodingKeys: String, CodingKey {
        case policyList = "policy_list"
    }
    
}
