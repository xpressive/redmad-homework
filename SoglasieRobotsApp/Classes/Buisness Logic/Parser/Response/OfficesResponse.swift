//
//  OfficesResponse.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 06.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

struct OfficesSuccessResponse: Codable {
    
    let data: OfficesResponse
    
}

struct OfficesResponse: Codable {
    
    let officeList: [Office]
    
    private enum CodingKeys: String, CodingKey {
        case officeList = "office_list"
    }
    
}
