//
//  DamagesResponse.swift
//  SoglasieRobotsApp
//
//  Created by Alex on 13.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

struct DamagesSuccessResponse: Codable {
    
    let data: DamagesResponse
    
}

struct DamagesResponse: Codable {
    
    let damageList: [Damage]
    
    private enum CodingKeys: String, CodingKey {
        case damageList = "damage_list"
    }
    
}

