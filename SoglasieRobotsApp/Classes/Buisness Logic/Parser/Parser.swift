//
//  Parser.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 06.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

final class Parser<T> where T: Codable {
    
    func parse(from data: Data) -> Result<T> {
        do {
            let model = try
                JSONDecoder().decode(T.self, from: data)
            return Result.success(payload: model)
        } catch {
            return Result.failure(error: error as NSError)
        }
    }
    
}
