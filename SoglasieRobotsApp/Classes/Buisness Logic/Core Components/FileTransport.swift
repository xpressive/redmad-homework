//
//  FileTransport.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 07.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation


///File Transport
final class FileTransport {
    
    // MARK: - Pubic Methods
    
    func request(for resource: String,
                 type: String,
                 completion: ((Result<Data>) -> Void)?) {
        guard let path = Bundle.main.path(forResource: resource, ofType: "json") else {
            assertionFailure("no json in that path")
            let error = NSError(domain: "no json file found", code: 0, userInfo: nil)
            completion?(Result.failure(error: error as NSError))
            return
        }
        let url = URL(fileURLWithPath: path)
        do {
            let data = try Data(contentsOf: url)
            completion?(Result.success(payload: data))
        } catch {
            completion?(Result.failure(error: error as NSError))
        }
    }
}
