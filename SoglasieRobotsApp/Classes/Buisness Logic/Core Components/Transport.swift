//
//  Transport.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 07.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Alamofire
import Foundation

/// Результат запроса транспорта
struct ResponseResult {
    let httpStatusCode: Int
    let headers: [AnyHashable: Any]
    let resultBody: Data
}

enum HTTPTransportMethod: String {
    case options = "OPTIONS"
    case get     = "GET"
    case head    = "HEAD"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
    case trace   = "TRACE"
    case connect = "CONNECT"
}

///Transport
final class Transport {
    
    // MARK: - Private Properties
    
    private let sessionManager: SessionManager
    
    // MARK: - Initialization
    
    init(sessionManager: SessionManager) {
        self.sessionManager = sessionManager
    }
    
    // MARK: - Pubic Methods
    
    /// Отправка запроса на сервер
    /// - Parameters:
    ///   - method: Метод
    ///   - parameters: Параметры
    ///   - timeout: Таймаут запроса - по дефолту 15 секунд
    ///   - headers: Хэдеры
    ///   - completion: Результат выполнения запроса
    func request(method: HTTPTransportMethod,
                 url: String,
                 parameters: Data? = nil,
                 timeout: TimeInterval = 15,
                 headers: [String: String] = [:],
                 completion: ((Result<ResponseResult>) -> Void)?) {
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = method.rawValue
        request.httpBody = parameters
        request.allHTTPHeaderFields = headers
        request.timeoutInterval = timeout
        sessionManager.request(request).responseData { [unowned self] response in
            switch response.result {
            case .success(let resultData):
                do {
                    self.debugPrint(data: resultData)
                    guard let statusCode = response.response?.statusCode,
                        let allHeaderFields = response.response?.allHeaderFields else { return }
                    let payload = ResponseResult(httpStatusCode: statusCode,
                                                          headers: allHeaderFields,
                                                          resultBody: resultData)
                    completion?(Result.success(payload: payload))
                }
            case .failure(let error):
                completion?(Result.failure(error: error as NSError))
            }
        }
    }
}

// MARK: - Debug Print

private extension Transport {
    
    func debugPrint(data: Data) {
        let json = try? JSONSerialization.jsonObject(with: data, options: [])
        print(json ?? "<null>")
    }
}

