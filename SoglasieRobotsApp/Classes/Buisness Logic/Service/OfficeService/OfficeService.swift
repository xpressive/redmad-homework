//
//  OfficeService.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 07.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

protocol OfficeService {
    
    /// Получить список офисов через мой transport
    /// - Parameter completion: Result запроса
    func obtainOfficeList(completion: ((Result<OfficesResponse>) -> Void)?)
    
    /// Получить список офисов через file transport
    /// - Parameter completion: Result запроса
    func obtainOfficeListFromJson(completion: ((Result<OfficesResponse>) -> Void)?)

}
