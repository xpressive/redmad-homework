//
//  OfficeServiceImpl.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 07.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

final class OfficeServiceImpl: OfficeService {
    
    // MARK: - Constants
    
    private enum Constants {
        static let baseURL = "https://soglasie-mock-lk.dev.redmadrobot.com/api/v1"
        static let resourcePath = "office_list"
    }
    
    private enum EndPoint {
        static let offices = "/offices"
    }
    
    
    // MARK: - Private Properties
    
    private let transport: Transport
    private let parser: Parser<OfficesSuccessResponse>
    private let fileTransport: FileTransport
    
    
    // MARK: - Initialization
    
    init(transport: Transport, fileTransport: FileTransport) {
        self.transport = transport
        self.fileTransport = fileTransport
        self.parser = Parser<OfficesSuccessResponse>()
    }
    
    func obtainOfficeList(completion: ((Result<OfficesResponse>) -> Void)?) {
        let resourceURI = Constants.baseURL + EndPoint.offices
        transport.request(method: .get,
                          url: resourceURI) { [unowned self] transportResult in
                            switch transportResult {
                            case .success(let payload):
                                let resultBody = payload.resultBody
                                let parseResult = self.parser.parse(from: resultBody)
                                switch parseResult {
                                case .success(let model):
                                    completion?(Result.success(payload: model.data))
                                case .failure(let error):
                                    completion?(Result.failure(error: error as NSError))
                                }
                            case .failure(let error):
                                // Информирование об ошибке UI слой
                                completion?(Result.failure(error: error as NSError))
                            }
        }
    }
    
    ///Метод для получение списка офисов из файла
    func obtainOfficeListFromJson(completion: ((Result<OfficesResponse>) -> Void)?) {
        fileTransport.request(for: Constants.resourcePath, type: "json") { [unowned self] transportResult in
            switch transportResult {
            case .success(let payload):
                let parseResult = self.parser.parse(from: payload)
                switch parseResult {
                case .success(let model):
                    completion?(Result.success(payload: model.data))
                case .failure(let error):
                    completion?(Result.failure(error: error as NSError))
                }
            case .failure(let error):
                // Информирование об ошибке UI слой
                completion?(Result.failure(error: error as NSError))
            }
        }
    }
    
}
