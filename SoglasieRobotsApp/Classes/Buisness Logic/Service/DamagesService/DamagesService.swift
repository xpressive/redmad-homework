//
//  DamagesService.swift
//  SoglasieRobotsApp
//
//  Created by Alex on 13.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation
import RxSwift

protocol DamagesService {
    
    /// Получить список полисов
    /// - return: Observable policies
    func obtainDamages() -> Observable<[Damage]>
    
}
