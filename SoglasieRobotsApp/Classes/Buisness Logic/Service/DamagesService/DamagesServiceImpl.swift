//
//  DamagesServiceImpl.swift
//  SoglasieRobotsApp
//
//  Created by Alex on 13.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation
import RxSwift


final class DamagesServiceImpl: DamagesService {
    
    // MARK: - Constants
    
    private enum Constants {
        static let baseURL = "https://soglasie-mock-lk.dev.redmadrobot.com/api/v1"
    }
    
    private enum EndPoint {
        static let damages = "/damages"
    }
    
    
    // MARK: - Private Properties
    
    private let transport: Transport
    private let parser: Parser<DamagesSuccessResponse>
    
    
    // MARK: - Initialization
    
    init(transport: Transport) {
        self.transport = transport
        self.parser = Parser<DamagesSuccessResponse>()
    }
    
    
    // MARK: - Pubic Methods
    
    func obtainDamages() -> Observable<[Damage]> {
        return Observable.create({ (observer) -> Disposable in
            let resourceURI = Constants.baseURL + EndPoint.damages
            self.transport.request(method: .get,
                                   url: resourceURI) { [unowned self] transportResult in
                                    switch transportResult {
                                    case .success(let payload):
                                        let resultBody = payload.resultBody
                                        let parseResult = self.parser.parse(from: resultBody)
                                        switch parseResult {
                                        case .success(let model):
                                            observer.onNext(model.data.damageList)
                                            observer.onCompleted()
                                        case .failure(let error):
                                            observer.onError(error as NSError)
                                        }
                                    case .failure(let error):
                                        observer.onError(error as NSError)
                                    }
            }
            return Disposables.create()
        })
    }
    
}


    

