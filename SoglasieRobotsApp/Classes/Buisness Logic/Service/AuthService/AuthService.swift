//
//  AuthService.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 29.01.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

typealias LoginCompletion = (User?, Error?) -> ()

protocol AuthService {

    /**
     Авторизация юзера по номеру телефона. В completion находится либо обьект юзера, либо ошибка
     */
    func loginUser(phone: String, password: String, completion:@escaping LoginCompletion)
    
}
