//
//  AuthServiceImpl.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 29.01.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation
import Alamofire
import KeychainAccess

enum BaseError: String, Error {
    case noData = "no_data_error"
    case parseError = "parse_json_error"
}

final class AuthServiceImpl: AuthService {
    
    private let sessionManager: SessionManager
    private let keychain: Keychain
    
    init(sessionManager: SessionManager, keychain: Keychain) {
        self.sessionManager = sessionManager
        self.keychain = keychain
    }
    
    func loginUser(phone: String, password: String,
                   completion:@escaping LoginCompletion) {
        
        let baseUrl = URL(string: "https://soglasie-mock-lk.dev.redmadrobot.com/api/v1/")!
        let loginUrl = baseUrl.appendingPathComponent("auth/login/phone")
        let headers = ["Content-Type": "application/json"]
        
        let params = ["phone": phone, "password": password]
        
        sessionManager.request(loginUrl,
                               method: .post,
                               parameters: params,
                               encoding: JSONEncoding.default,
                               headers: headers)
            .responseData { (response) in
                
                guard let data = response.result.value else {
                    completion(nil, BaseError.noData)
                    return
                }
                do {
                    let successResponse = try
                        JSONDecoder().decode(SuccessResponse.self, from: data)
                    self.keychain["token"] = successResponse.data.accessToken
                    self.keychain["phone"] = phone
                    self.keychain["password"] = password
                    completion(successResponse.data.user, nil)
                } catch {
                    completion(nil, BaseError.parseError)
                }
                
        }
        
    }
    
    
    
}

