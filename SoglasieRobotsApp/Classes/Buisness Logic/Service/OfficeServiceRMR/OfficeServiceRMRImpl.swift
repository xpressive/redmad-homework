//
//  OfficeServiceRMRImpl.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 06.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation
import HTTPTransport


final class OfficeServiceRMRImpl: OfficeServiceRMR {
    
    // MARK: - Constants
    
    private enum Constants {
        static let baseURL = "https://soglasie-mock-lk.dev.redmadrobot.com/api/v1"
    }
    
    private enum EndPoint {
        static let offices = "/offices"
    }
    
    
    // MARK: - Private Properties
    
    private let transport: HTTPTransport
    private let parser: Parser<OfficesSuccessResponse>
    
    // MARK: - Initialization
    
    init(transport: HTTPTransport) {
        self.transport = transport
        self.parser = Parser<OfficesSuccessResponse>()
    }
    
    
    // MARK: - Pubic Methods
    
    func obtainOfficeList(completion: ((Result<OfficesResponse>) -> Void)?) {
        let resourceURI = Constants.baseURL + EndPoint.offices
        let request: HTTPRequest = HTTPRequest(httpMethod: HTTPRequest.HTTPMethod.get, endpoint: resourceURI)
        let queue = OperationQueue()
        queue.qualityOfService = .background
        queue.addOperation {
            let result: HTTPTransport.Result = self.transport.send(request: request)
            
            print("")
            print("RESULTS")
            print("")
            
            switch result {
            case .success(let response):
                debugPrint(try! response.getJSONDictionary()!)
                guard let body = response.body else {
                    assertionFailure("Response have no body")
                    completion?(Result.failure(error: NSError(domain: "no response body",
                                                                         code: 404,
                                                                         userInfo: nil)))
                    return
                }
                let result = self.parser.parse(from: body)
                switch result {
                case .success(let model):
                    completion?(Result.success(payload: model.data))
                case .failure(let error):
                    completion?(Result.failure(error: error as NSError))
                }
            case .failure(let error):
                completion?(Result.failure(error: error as NSError))
            }
            print("")
        }

    }
    
}
