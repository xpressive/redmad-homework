//
//  OfficeServiceRMR.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 06.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

protocol OfficeServiceRMR {
    
    /// Получить список офисов через билиотеку http transport
    /// - Parameter completion: Result запроса
    func obtainOfficeList(completion: ((Result<OfficesResponse>) -> Void)?)
    
}
