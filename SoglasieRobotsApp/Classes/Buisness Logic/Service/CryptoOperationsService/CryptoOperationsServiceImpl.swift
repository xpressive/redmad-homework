//
//  CryptoOperationsServiceImpl.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 21.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation
import KeychainAccess
import RNCryptor

final class CryptoOperationsServiceImpl: CryptoOperationsService {
    
    private let keychain: Keychain
    
    
    init(keychain: Keychain) {
        self.keychain = keychain
    }
    
    func hasBeenAuthorized() -> Bool {
        if let _ = keychain[data: "encrypted_token"],
        let _ = keychain[data: "hmac_key"],
            let _ = keychain[data: "salt"] {
            return true
        } else {
            return false
        }
    }
    
    func pinMatches(pin: String) -> Bool {
        guard let pinCode = keychain["pin_code"] else {
            return false
        }
        return pin == pinCode
    }
    
    func setPincode(pin: String) {
        
        let salt = RNCryptor.randomData(ofLength: RNCryptor.FormatV3.saltSize)
        let key = RNCryptor.FormatV3.makeKey(forPassword: pin, withSalt: salt)
        let hmacKey = RNCryptor.randomData(ofLength: RNCryptor.FormatV3.keySize)
        guard let token = keychain["token"]?.data(using: .utf8) else {
            return
        }
        
        let encryptor = RNCryptor.EncryptorV3(encryptionKey: key, hmacKey: hmacKey)
        
        let cipherText = Data(encryptor.encrypt(data: token))
        
        keychain[data: "salt"] = salt
        keychain[data: "hmac_key"] = hmacKey
        keychain[data: "encrypted_token"] = cipherText
        keychain["pin_code"] = pin
        
    }
    
    func getSessionToken(pin: String) -> String? {
        guard let salt = keychain[data: "salt"],
        let hmacKey = keychain[data: "hmac_key"],
            let encryptedToken = keychain[data: "encrypted_token"] else {
                return nil
        }
        
        let key = RNCryptor.FormatV3.makeKey(forPassword: pin, withSalt: salt)
        
        do {
            let token = try RNCryptor.DecryptorV3(encryptionKey: key, hmacKey: hmacKey).decrypt(data: encryptedToken)
            return String(data: token, encoding: .utf8)
        } catch {
            return nil
        }
    }
    
    
    func getSessionTokenFromTouchId() -> String? {
        if let pinExist = keychain["pin_code"] {
            return self.getSessionToken(pin: pinExist)
        } else {
            return nil
        }
    }
    
    func deleteAllSecrets() {
        keychain[data: "salt"] = nil
        keychain[data: "hmac_key"] = nil
        keychain[data: "encrypted_token"] = nil
        keychain["pin_code"] = nil
    }
    
}
