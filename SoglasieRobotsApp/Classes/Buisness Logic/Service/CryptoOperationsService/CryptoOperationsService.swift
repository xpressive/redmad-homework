//
//  CryptoOperationsService.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 21.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

protocol CryptoOperationsService {
    
    func setPincode(pin: String)
    func pinMatches(pin: String) -> Bool
    func getSessionToken(pin: String) -> String?
    func getSessionTokenFromTouchId() -> String?
    func deleteAllSecrets()
    func hasBeenAuthorized() -> Bool
    
}
