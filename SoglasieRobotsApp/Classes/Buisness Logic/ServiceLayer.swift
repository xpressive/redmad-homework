//
//  ServiceLayer.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 29.01.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation
import Alamofire
import HTTPTransport
import KeychainAccess

final class ServiceLayer {
    
    static let shared = ServiceLayer()
    
    let sessionManager: SessionManager
    let sessionDelegate: SessionDelegate
    let delegateQueue: OperationQueue
    let transportRMR: HTTPTransport
    let transport: Transport
    let fileTransport: FileTransport
    let keychain: Keychain
    let headersAdapter: SoglasieHeadersAdapter
    let backgroundTimeService: TimeInBackgroundCalculatorService
    
    let authService: AuthService
    let officeServiceRMR: OfficeServiceRMR
    let officeService: OfficeService
    let policyService: PolicyService
    let damagesService: DamagesService
    let cryptoService: CryptoOperationsService
    
    init() {
        sessionDelegate = SessionDelegate()
        delegateQueue = OperationQueue()
        keychain = Keychain()
        
        let configuration = URLSessionConfiguration.ephemeral
        // На всякий случай зачищаю все что можно
        configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        configuration.urlCache = nil
        configuration.httpCookieAcceptPolicy = .never
        configuration.httpCookieStorage = nil
        configuration.httpShouldSetCookies = false
        let session = URLSession(configuration: configuration,
                                 delegate: sessionDelegate,
                                 delegateQueue: delegateQueue)
        
        sessionManager = SessionManager(session: session,
                                        delegate: sessionDelegate)!
        sessionManager.adapter = SoglasieHeadersAdapter()
        
        let sessionRMR = Session(manager: sessionManager)
        transportRMR = HTTPTransport(session: sessionRMR)
        transport = Transport(sessionManager: sessionManager)
        fileTransport = FileTransport()
        
        authService = AuthServiceImpl(sessionManager: sessionManager, keychain: keychain)
        officeServiceRMR = OfficeServiceRMRImpl(transport: transportRMR)
        officeService = OfficeServiceImpl(transport: transport, fileTransport: fileTransport)
        policyService = PolicyServiceImpl(transport: transport)
        damagesService = DamagesServiceImpl(transport: transport)
        cryptoService = CryptoOperationsServiceImpl(keychain: keychain)
        backgroundTimeService = TimeInBackgroundCalculatorService()

        // Отключил пролонгацию
//        let retrier = AuthHandler(authService: authService, keychain: keychain)
//        sessionManager.retrier = retrier
        
        headersAdapter = SoglasieHeadersAdapter()
        
    }
}
