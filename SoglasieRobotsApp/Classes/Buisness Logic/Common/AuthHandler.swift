//
//  AuthHandler.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 13.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation
import Alamofire
import KeychainAccess

class AuthHandler: RequestRetrier {
    
    private let authService: AuthService
    private let keychain: Keychain
    
    init(authService: AuthService, keychain: Keychain) {
        self.authService = authService
        self.keychain = keychain
    }
    
    public func should(_ manager: SessionManager,
                       retry request: Request,
                       with error: Error,
                       completion: @escaping RequestRetryCompletion) {
        
        if let response = request.task?.response as? HTTPURLResponse, response.statusCode == 403 {
            keychain["token"] = nil
            guard let phone = keychain["phone"],
                let password = keychain["password"] else {
                    completion(false, 0.0)
                    return
            }
            authService.loginUser(phone: phone, password: password, completion: { (user, error) in
                completion(false, 0.0)
            })
            
        } else {
            completion(false, 0.0)
        }
    }
}
