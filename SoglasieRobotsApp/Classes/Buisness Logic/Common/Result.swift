//
//  Result.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 07.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(payload: T)
    case failure(error: Error)
}
