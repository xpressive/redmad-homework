//
//  SoglasieHeadersAdapter.swift
//  SoglasieRobotsApp
//
//  Created by Alex on 13.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation
import Alamofire


final class SoglasieHeadersAdapter: RequestAdapter {
    
    var token: String?
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        
        var urlRequest = urlRequest
        if let tokenExist = token {
            urlRequest.setValue(tokenExist, forHTTPHeaderField: "access-token")
        }
        
        return urlRequest
    }
    
}
