//
//  PresentationModel.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 29.01.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

class PresentationModel {
    
    enum State {
        case rich
        case loading
        case error(message: String)
    }
    
    typealias ChangeStateHandler = (State) -> Void
    
    var changeStateHandler: ChangeStateHandler?
    var state: State = .rich {
        didSet { changeStateHandler?(state) }
    }
    
}
