//
//  BaseViewController.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 07.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    // MARK: - Constants
    
    private enum BaseConstants {
        
        /// Localization
        static let alertOk = NSLocalizedString("alert_ok_button", comment: "")
        
    }

    func showErrorAlert(title: String, message: String) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: BaseConstants.alertOk, style: .default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

}
