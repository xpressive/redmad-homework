//
//  OfficeListPresentationModel.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 07.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

final class OfficeListPresentationModel: PresentationModel {
    
    // MARK: - Public Properties
    
    /// Список офисов
    var viewModels = [OfficeViewModel]()
    
    
    // MARK: - Private properties
    /// Несортированный список офисов, который ждет получения локации
    private var unsortedViewModels = [OfficeViewModel]()
    
    // Последняя известная локация юзера
    private var lastKnownLocation: CLLocation?
    
    /// Сервисы для офисов
    private let officeService = ServiceLayer.shared.officeService
    private let officeServiceRMR = ServiceLayer.shared.officeServiceRMR
    
    
    // Метод который дергает контроллер когда получает локацию
    func locationRecieved(myLocation: CLLocation) {
        if unsortedViewModels.count == 0 {
            lastKnownLocation = myLocation
        } else {
            viewModels = unsortedViewModels.sorted(by: { $0.distance(to: myLocation) < $1.distance(to: myLocation) })
            state = .rich
        }
        
    }
    
    /// Метод для загрузки координат офисов
    func obtainOffices(from source: SourceType) {
        state = .loading
        switch source {
        case .transport:
            officeService.obtainOfficeList(completion: { [weak self] (result) in
                self?.obtainOffices(result: result)
            })
        case .transportRMR:
            officeServiceRMR.obtainOfficeList(completion: { [weak self] (result) in
                self?.obtainOffices(result: result)
            })
        case .jsonFile:
            officeService.obtainOfficeListFromJson(completion: { [weak self] (result) in
                self?.obtainOffices(result: result)
            })
        }
    }
    
    // MARK: - Private methods
    
    private func obtainOffices(result: Result<OfficesResponse>) {
        
        switch result {
        case .success(payload: let model):
            unsortedViewModels = model.officeList.map { OfficeViewModel($0) }
            if let loc = lastKnownLocation {
                self.locationRecieved(myLocation: loc)
            }
        case .failure(error: let error):
            self.state = .error(message: error.localizedDescription)
        }
    }
        
}
