//
//  UserInfoPresentationModel.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 30.01.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

final class UserInfoPresentationModel: PresentationModel {
    
    // Services
    private let cryptoService = ServiceLayer.shared.cryptoService
    private let headersAdapter = ServiceLayer.shared.headersAdapter
    
    func clearAuthData() {
        cryptoService.deleteAllSecrets()
        headersAdapter.token = nil
    }
    
}
