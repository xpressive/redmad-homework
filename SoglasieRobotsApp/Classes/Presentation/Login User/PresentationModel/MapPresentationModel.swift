//
//  MapPresentationModel.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 06.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

final class MapPresentationModel: PresentationModel {
    
    // MARK: - Private properties
    
    
    /// Сервисы для офисов
    private let officeService = ServiceLayer.shared.officeService
    private let officeServiceRMR = ServiceLayer.shared.officeServiceRMR
    
    /// Метод для загрузки координат офисов
    func obtainOffices(from source: SourceType, completion: (([CoordinateMapViewModel]) -> ())?) {
        state = .loading
        switch source {
        case .transport:
            officeService.obtainOfficeList(completion: { [weak self] (result) in
                self?.obtainOffices(result: result, completion: completion)
            })
        case .transportRMR:
            officeServiceRMR.obtainOfficeList(completion: { [weak self] (result) in
                self?.obtainOffices(result: result, completion: completion)
            })
        case .jsonFile:
            officeService.obtainOfficeListFromJson(completion: { [weak self] (result) in
                self?.obtainOffices(result: result, completion: completion)
            })
        }
    }
    
    // MARK: - Private methods
    
    private func obtainOffices(result: Result<OfficesResponse>,
                               completion: (([CoordinateMapViewModel]) -> ())?) {
        
        switch result {
        case .success(payload: let model):
            self.state = .rich
            completion?(model.officeList.map { CoordinateMapViewModel($0.coordinate) })
        case .failure(error: let error):
            self.state = .error(message: error.localizedDescription)
            completion?([])
        }
        
    }
    
    
}
