//
//  AuthorizationPresentationModel.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 29.01.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation   

final class AuthorizationPresentationModel: PresentationModel {

    // MARK: - Private properties
    private var user: User?
    
    /// Сервис для авторизации пользователя
    private let authService = ServiceLayer.shared.authService
    
    private let cryptoService = ServiceLayer.shared.cryptoService
    
    /// Метод для авторизации пользователя
    func authenticateUser(phoneNumber: String,
                          password: String,
                          completion: @escaping (UserViewModel?) -> ()) {
        state = .loading
        authService.loginUser(phone: phoneNumber, password: password) { [weak self] (user, error) in
            guard let strongSelf = self else { return }
            if let userExists = user {
                strongSelf.state = .rich
                completion(UserViewModel.init(userExists))
            } else {
                let err = error as! BaseError
                strongSelf.state = State.error(message: err.rawValue)
                completion(nil)
            }
        }
    }
    
    func isUserAuthorized() -> Bool {
        return cryptoService.hasBeenAuthorized()
    }
    
}
