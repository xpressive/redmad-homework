//
//  PasscodePresentationModel.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 21.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

final class PasscodePresentationModel: PresentationModel {
    
    
    //MARK: - Private Properties
    
    private let cryptoService = ServiceLayer.shared.cryptoService
    private let headersAdapter = ServiceLayer.shared.headersAdapter
    
    private let type: PasscodeViewType
    
    private var incorrectAttempsNumber = 0
    
    init(type: PasscodeViewType) {
        self.type = type
    }
    
    func handleSuccess() {
        
        /// Токен теперь хранится не в кейчейне а в памяти
        if let token = self.cryptoService.getSessionTokenFromTouchId() {
            headersAdapter.token = token
        }
        self.state = .rich
    }
    
    func handleFailure() {
        if type == .checkPassword {
            incorrectAttempsNumber += 1
            if incorrectAttempsNumber == 5 {
                self.cryptoService.deleteAllSecrets()
                self.state = .error(message: "Max incorrect attemps")
            }
        }
    }
    
    func set(password: String) -> ValidatePasswordModel? {
        self.cryptoService.setPincode(pin: password)
        return ValidatePasswordModel()
    }
    
    func match(password: String) -> ValidatePasswordModel? {
        if let _ = cryptoService.getSessionToken(pin: password) {
            return ValidatePasswordModel()
        } else {
            return nil
        }
    }
    
    
    
}
