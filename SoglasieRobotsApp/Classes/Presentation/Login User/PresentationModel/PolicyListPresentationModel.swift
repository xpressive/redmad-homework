//
//  PolicyListPresentationModel.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 13.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation
import RxSwift

final class PolicyListPresentationModel: PresentationModel {
    
    // MARK: - Public Properties
    
    /// Список полисов
    var viewModels = [PolicyViewModel]()
    
    // MARK: - Private properties
    /// список Damages
    private var damageViewModel = [DamageViewModel]()
    
    
    /// Сервисы для полисов и повреждений
    private let policyService = ServiceLayer.shared.policyService
    private let damageService = ServiceLayer.shared.damagesService
    
    private let disposeBag = DisposeBag()
    
    func obtainPoliciesAndDamages() {
        state = .loading
        Observable.zip(policyService.obtainPolicies(), damageService.obtainDamages()) { [weak self] polices, damages in
            self?.viewModels = polices.map { PolicyViewModel($0) }
            self?.damageViewModel = damages.map { DamageViewModel($0) }
        }
        .retry(4)
        .observeOn(MainScheduler.instance)
        .subscribe { [weak self] (event) in
            switch event {
            case .next:
                debugPrint("next")
            case .error(let error):
                self?.state = .error(message: error.localizedDescription)
            case .completed:
                self?.state = .rich
            }
        }.disposed(by: disposeBag)
    }
    
    func obtainDamagesForPoliceAt(index: Int) -> [DamageViewModel] {
        return damageViewModel.filter { $0.policyType == viewModels[index].type }
    }
    
}
