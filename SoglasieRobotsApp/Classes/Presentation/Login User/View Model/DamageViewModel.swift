//
//  DamageViewModel.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 13.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

struct DamageViewModel {
    
    let id: Int
    let policyTitle: String
    let policyType: PoliceType
    let policyNumber: String
    let number: String
    let type: PoliceType
    
    init(_ damage: Damage) {
        self.id = damage.id
        self.policyTitle = damage.policyTitle
        self.policyType = damage.policyType
        self.policyNumber = damage.policyNumber
        self.number = damage.number
        self.type = damage.policyType
    }
}
