//
//  UserViewModel.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 30.01.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

struct UserViewModel {
    
    let phone: String
    let firstName: String?
    let secondName: String?
    
    init(_ user: User) {
        self.phone = user.phone
        self.firstName = user.firstName
        self.secondName = user.secondName
    }
}
