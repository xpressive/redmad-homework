//
//  CoordinateMapViewModel.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 06.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

final class CoordinateMapViewModel: NSObject, GMUClusterItem {
    
    var position: CLLocationCoordinate2D
    
    init(_ coordinate: Coordinate) {
        self.position = CLLocationCoordinate2D(latitude: coordinate.lat,
                                               longitude: coordinate.lng)
    }
}
