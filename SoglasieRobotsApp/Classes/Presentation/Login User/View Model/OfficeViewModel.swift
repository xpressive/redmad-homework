//
//  OfficeViewModel.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 07.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

struct OfficeViewModel {
    
    let city: String
    let adress: String
    let coordinate: CoordinateViewModel
    
    init(_ office: Office) {
        self.city = office.city
        self.adress = office.address
        self.coordinate = CoordinateViewModel(office.coordinate)
    }
    
    private var location: CLLocation {
        return CLLocation(latitude: self.coordinate.lat, longitude: self.coordinate.lng)
    }
    
    func distance(to location: CLLocation) -> CLLocationDistance {
        return location.distance(from: self.location)
    }
}
