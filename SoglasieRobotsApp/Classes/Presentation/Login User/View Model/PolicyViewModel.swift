//
//  PolicyViewModel.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 13.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

struct PolicyViewModel {
    
    let id: Int
    let title: String
    let number: String
    let type: PoliceType
    
    init(_ policy: Policy) {
        self.id = policy.id
        self.title = policy.title
        self.number = policy.number
        self.type = policy.type
    }
    
}

