//
//  CoordinateViewModel.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 07.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

struct CoordinateViewModel {
    
    let lat: Double
    let lng: Double
    
    init(_ coordinate: Coordinate) {
        lat = coordinate.lat
        lng = coordinate.lng
    }
    
}
