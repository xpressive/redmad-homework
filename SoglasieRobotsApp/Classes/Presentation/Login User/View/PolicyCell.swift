//
//  PolicyCell.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 13.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

final class PolicyCell: UITableViewCell {
    
    
    //MARK: - Constants
    private enum Constants {
        
        /// Localization
        static let idDescriptionText = NSLocalizedString("policy_id_description", comment: "")
        static let titleDescription = NSLocalizedString("policy_title_description", comment: "")
        static let numberDescription = NSLocalizedString("policy_number_description", comment: "")
    
    }
    
    
    //MARK: - IBOutlet
    
    @IBOutlet var idDescriptionLabel: UILabel!
    @IBOutlet var titleDescriptionLabel: UILabel!
    @IBOutlet var numberDescriptionLabel: UILabel!
    
    @IBOutlet var idLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var numberLabel: UILabel!
    
    // MARK: - Configure
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configure()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
    }
    
    private func configure() {
        idLabel.textColor = .black
        titleLabel.textColor = .black
        backgroundColor = .clear
        
        titleDescriptionLabel.text = Constants.titleDescription
        numberDescriptionLabel.text = Constants.numberDescription
        idDescriptionLabel.text = Constants.idDescriptionText
    }

    // MARK: - ViewModelConfigurable
    
    func configure(for viewModel: PolicyViewModel) {
        idLabel.text = "\(viewModel.id)"
        titleLabel.text = viewModel.title
        numberLabel.text = viewModel.number
    }
    
    func clear() {
        idLabel.text = nil
        titleLabel.text = nil
        numberLabel.text = nil
    }
    
}

