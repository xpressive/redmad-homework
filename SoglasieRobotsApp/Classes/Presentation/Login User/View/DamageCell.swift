//
//  DamageCell.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 14.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

final class DamageCell: UITableViewCell {
    
    // MARK: - IBOutlets
    
    @IBOutlet var idLabel: UILabel!
    @IBOutlet var policeTitleLabel: UILabel!
    @IBOutlet var policeNumberLabel: UILabel!
    @IBOutlet var numberLabel: UILabel!
    
    
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configure()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
    }
    
    
    // MARK: - Private
    
    private func configure() {
        idLabel.textColor = .black
        policeTitleLabel.textColor = .black
        policeNumberLabel.textColor = .black
        numberLabel.textColor = .black
        
        backgroundColor = .clear
    }
    
    
    // MARK: - ViewModelConfigurable
    
    func configure(for viewModel: DamageViewModel) {
        idLabel.text = "\(viewModel.id)"
        policeTitleLabel.text = viewModel.policyTitle
        policeNumberLabel.text = viewModel.policyNumber
        numberLabel.text = viewModel.number
    }
    
    func clear() {
        idLabel.text = nil
        policeTitleLabel.text = nil
        policeNumberLabel.textColor = nil
        numberLabel.text = nil
    }
    
    
}
