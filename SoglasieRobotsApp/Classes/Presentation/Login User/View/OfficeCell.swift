//
//  OfficeCell.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 07.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import UIKit


final class OfficeCell: UITableViewCell {
    
    //MARK: - IBOutlet
    
    @IBOutlet var cityLabel: UILabel!
    @IBOutlet var adressLabel: UILabel!
    
    // MARK: - Configure
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configure()
    }
    
    private func configure() {
        cityLabel.textColor = .black
        adressLabel.textColor = .black
        backgroundColor = .clear
        selectionStyle = .none
    }
    
    
    // MARK: - ViewModelConfigurable
    
    func configure(for viewModel: OfficeViewModel) {
        cityLabel.text = viewModel.city
        adressLabel.text = viewModel.adress
    }
    
    func clear() {
        cityLabel.text = nil
        adressLabel.text = nil
    }
    
}

