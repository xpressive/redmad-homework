//
//  PasscodeViewController.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 20.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation
import SmileLock

enum PasscodeViewType {
    case setPassword
    case checkPassword
}

final class PasscodeViewController: UIViewController {
    
    //MARK: - Constants
    private enum Constants {
        static let showUserInfoSegueId = "ShowUserInfoSegue"
    }
    
    //MARK: - Public Properties
    var passcodeViewType: PasscodeViewType = .setPassword
    var userViewModel: UserViewModel?
    
    //MARK: - Outlets
    @IBOutlet weak var passwordStackView: UIStackView!
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK: - Private Properties
    private var passwordUIValidation: PasswordValidation!
    private var presentationModel: PasscodePresentationModel!
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presentationModel = PasscodePresentationModel(type: passcodeViewType)
        // Немного костылей чтобы библиотека работала с MVPM архитектурой
        passwordUIValidation = PasswordValidation(in: passwordStackView) { [weak self] password in
            guard let strongSelf = self else { return nil }
            switch strongSelf.passcodeViewType {
            case .setPassword:
                return strongSelf.presentationModel.set(password: password)
            case .checkPassword:
                return strongSelf.presentationModel.match(password: password)
            }
            
        }
        
        self.bindEvents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        switch self.passcodeViewType {
        case .setPassword:
            self.passwordUIValidation.view.touchAuthenticationEnabled = false
            self.titleLabel.text = "Установите пин код"
        case .checkPassword:
            self.titleLabel.text = "Введите пин код"
            self.passwordUIValidation.view.touchAuthenticationEnabled = true
        }
        
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.showUserInfoSegueId {
            guard let mainVC = segue.destination as? UserInfoViewController else {
                assertionFailure()
                return
            }
            mainVC.userViewModel = userViewModel
        }
    }
    
    
    //MARK: - Private Methods
    
    
    private func bindEvents() {
        passwordUIValidation.success = { [weak self] model in
            self?.presentationModel.handleSuccess()
        }
        passwordUIValidation.failure = { [weak self] in
            self?.presentationModel.handleFailure()
        }
        presentationModel.changeStateHandler = { status in
            switch status {
            case .rich:
                self.showMainScreen()
            case .error(let _):
                self.navigationController?.popToRootViewController(animated: true)
            default:
                break
            }
        }
    }
    
    private func showMainScreen() {
        self.performSegue(withIdentifier: Constants.showUserInfoSegueId, sender: nil)
    }
    
    
}

//MARK: - Костыльная моделька для работы библиотеки с MVPM
class ValidatePasswordModel { }

class PasswordValidation: PasswordUIValidation<ValidatePasswordModel> {
    
    init(in stackView: UIStackView, completion:((_ password: String) -> ValidatePasswordModel?)?) {
        super.init(in: stackView, digit: 4)
        validation = { password in
            if let model = completion?(password) {
                return model
            } else {
                return nil
            }
        }
    }
    
    //handle Touch ID
    override func touchAuthenticationComplete(_ passwordContainerView: PasswordContainerView, success: Bool, error: Error?) {
        if success {
            let dummyModel = ValidatePasswordModel()
            self.success?(dummyModel)
        } else {
            passwordContainerView.clearInput()
        }
    }
}

