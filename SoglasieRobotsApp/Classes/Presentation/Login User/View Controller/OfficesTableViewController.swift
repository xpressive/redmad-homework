//
//  OfficesTableViewController.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 07.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import UIKit
import MBProgressHUD
import RxSwift
import RxCocoa

class OfficesTableViewController: BaseViewController {

    // MARK: - Constants
    
    private enum Constants {
        static let title = NSLocalizedString("offices_list_title", comment: "")
        static let cellIdentifier = String(describing: OfficeCell.self)
        
        static let alertTitle = NSLocalizedString("offices_source_alert_title", comment: "")
        static let alertMessage = NSLocalizedString("offices_source_alert_text", comment: "")
        static let sourceTypeTransport = NSLocalizedString("map_source_transport", comment: "")
        static let sourceTypeRMRTransport = NSLocalizedString("map_source_transport_rmr", comment: "")
        static let sourceTypeJSON = NSLocalizedString("map_source_json", comment: "")
        
        static let errorAlertTitle = NSLocalizedString("offices_list_alert_title", comment: "")
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet var tableView: UITableView!
    
    
    // MARK: - Public Properties
    let presentationModel = OfficeListPresentationModel()
    
    
    // MARK: - Private Properties
    private var locationManager = GeolocationService()
    private var disposeBag: DisposeBag!
    
    // MARK: - ViewController lyfecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.disposeBag = DisposeBag()
        configureUI()
        bindEvents()
        startLocationUpdates()
        showSourceSelectionAlert()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.disposeBag = nil
    }
    
    
    // MARK: - Private Methods
    
    /// Настройка UI
    private func configureUI() {
        title = Constants.title
        
        tableView.dataSource = self
        tableView.register(UINib(nibName: Constants.cellIdentifier, bundle: nil), forCellReuseIdentifier: Constants.cellIdentifier)
    }
    
    /// Подписка на события обновления Presentation Model
    private func bindEvents() {
        presentationModel.changeStateHandler = { [unowned self] status in
            switch status {
            case .loading:
                DispatchQueue.main.async {
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                }
            case .error(message: let message):
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.showErrorAlert(title: Constants.errorAlertTitle,
                                        message: message)
                }
                
                print(message)
            case .rich:
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.tableView.reloadData()
                }
            }
        }
        
    }
    
    /// Выбор источника загрузки
    private func showSourceSelectionAlert() {
        
        let alert = UIAlertController(title: Constants.alertTitle,
                                      message: Constants.alertMessage,
                                      preferredStyle: .alert)
        
        let transportAction = UIAlertAction(title: Constants.sourceTypeTransport,
                                            style: .default) { (action) in
                                                self.obtainData(from: .transport)
        }
        let transportRMRAction = UIAlertAction(title: Constants.sourceTypeRMRTransport,
                                               style: .default) { (action) in
                                                self.obtainData(from: .transportRMR)
        }
        let transportJSONAction = UIAlertAction(title: Constants.sourceTypeJSON,
                                                style: .default) { (action) in
                                                    self.obtainData(from: .jsonFile)
        }
        alert.addAction(transportAction)
        alert.addAction(transportRMRAction)
        alert.addAction(transportJSONAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    /// Спрашиваем юзера про локацию
    private func startLocationUpdates() {
        locationManager.location.asObservable().subscribe(onNext: { (location) in
            self.presentationModel.locationRecieved(myLocation: location)
        }, onError: { (error) in
            debugPrint(error.localizedDescription)
        }, onCompleted: {
            debugPrint("completed")
        }) {
            self.locationManager.stopUpdating()
        }.disposed(by: disposeBag)
    }
    
    /// Загрузка данных
    private func obtainData(from source: SourceType) {
        self.presentationModel.obtainOffices(from: source)
    }
    
}


// MARK: - UITableViewDataSource

extension OfficesTableViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presentationModel.viewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = presentationModel.viewModels[indexPath.row]
        let cellIdentifier = Constants.cellIdentifier
        
        guard let cell =
            tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? OfficeCell
            else { preconditionFailure() }
        
        cell.configure(for: viewModel)
        
        cell.layoutIfNeeded()
        
        return cell
    }
    
}
