//
//  PolicyTableViewController.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 13.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import UIKit
import MBProgressHUD

class PolicyTableViewController: BaseViewController {
    
    // MARK: - Constants
    
    private enum Constants {
        static let cellIdentifier = String(describing: PolicyCell.self)
        
        /// Localization
        static let errorAlertTitle = NSLocalizedString("offices_list_alert_title", comment: "")
        static let noDamagesErrorTitle = NSLocalizedString("damages_not_found_alert_title", comment: "")
        static let noDamagesErrorDescription = NSLocalizedString("damages_not_found_alert_description", comment: "")
        
        static let showDamagesSegue = "ShowDamagesSegue"
        
        static let rowHeight: CGFloat = 140
        
    }
    
    // MARK: - IBOutlets
    @IBOutlet var tableView: UITableView!
    
    
    // MARK: - Public Properties
    let presentationModel = PolicyListPresentationModel()
    
    
    // MARK: - Private Properties
    var damagesToShow: [DamageViewModel]!
    
    // MARK: - ViewController lyfecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        bindEvents()
        obtainData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.showDamagesSegue {
            guard let damagesVC = segue.destination as? DamagesTableViewController else {
                assertionFailure("Missed damages vc")
                return
            }
            damagesVC.damageViewModels = damagesToShow
        }
    }
    
    // MARK: - Private Methods
    
    /// Настройка UI
    private func configureUI() {
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: Constants.cellIdentifier, bundle: nil), forCellReuseIdentifier: Constants.cellIdentifier)
        tableView.rowHeight = Constants.rowHeight
    }
    
    /// Подписка на события обновления Presentation Model
    private func bindEvents() {
        presentationModel.changeStateHandler = { [unowned self] status in
            switch status {
            case .loading:
                MBProgressHUD.showAdded(to: self.view, animated: true)
            case .error(message: let message):
                MBProgressHUD.hide(for: self.view, animated: true)
                self.showErrorAlert(title: Constants.errorAlertTitle,
                                    message: message)
            case .rich:
                MBProgressHUD.hide(for: self.view, animated: true)
                self.tableView.reloadData()
            }
        }
    }
    
    /// Загрузка данных
    private func obtainData() {
        self.presentationModel.obtainPoliciesAndDamages()
    }
    
}


// MARK: - UITableViewDataSource

extension PolicyTableViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presentationModel.viewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = presentationModel.viewModels[indexPath.row]
        let cellIdentifier = Constants.cellIdentifier
        
        guard let cell =
            tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? PolicyCell
            else { preconditionFailure() }
        
        cell.configure(for: viewModel)
        
        cell.layoutIfNeeded()
        
        return cell
    }
    
}

extension PolicyTableViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let damages = presentationModel.obtainDamagesForPoliceAt(index: indexPath.row)
        if damages.count != 0 {
            damagesToShow = damages
            performSegue(withIdentifier: Constants.showDamagesSegue, sender: self)
        } else {
            self.showErrorAlert(title: Constants.noDamagesErrorTitle,
                                message: Constants.noDamagesErrorDescription)
        }
        
    }
    
}
