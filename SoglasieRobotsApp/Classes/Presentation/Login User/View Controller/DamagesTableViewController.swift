//
//  DamagesViewController.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 13.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import UIKit

class DamagesTableViewController: BaseViewController {

    // MARK: - Constants
    
    private enum Constants {
        static let cellIdentifier = String(describing: DamageCell.self)
        
        static let rowHeight: CGFloat = 145
    }
    
    
    // MARK: - IBOutlets
    @IBOutlet var tableView: UITableView!
    
    
    // MARK: - Public Properties
    
    var damageViewModels: [DamageViewModel] = []
    
    
    // MARK: - ViewController lyfecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    
    // MARK: - Private Methods
    func configureUI() {
        tableView.dataSource = self
        tableView.register(UINib(nibName: Constants.cellIdentifier, bundle: nil), forCellReuseIdentifier: Constants.cellIdentifier)
        tableView.rowHeight = Constants.rowHeight
    }
}

extension DamagesTableViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return damageViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let viewModel = damageViewModels[indexPath.row]
        let cellIdentifier = Constants.cellIdentifier
        
        guard let cell =
            tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? DamageCell
            else { preconditionFailure() }
        
        cell.configure(for: viewModel)
        
        cell.layoutIfNeeded()
        
        return cell
    }
    
}
