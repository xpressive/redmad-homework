//
//  MapViewController.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 06.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import UIKit
import GoogleMaps
import MBProgressHUD

final class MapViewController: BaseViewController {
    
    // MARK: - Constants
    
    private enum Constants {
        
        /// Localization
        static let alertTitle = NSLocalizedString("map_source_alert_title", comment: "")
        static let alertMessage = NSLocalizedString("map_source_alert_text", comment: "")
        static let sourceTypeTransport = NSLocalizedString("map_source_transport", comment: "")
        static let sourceTypeRMRTransport = NSLocalizedString("map_source_transport_rmr", comment: "")
        static let sourceTypeJSON = NSLocalizedString("map_source_json", comment: "")
        
        static let errorAlertTitle = NSLocalizedString("map_alert_title", comment: "")
    }
    
    // MARK: - Public Properties
    
    let presentationModel = MapPresentationModel()
    
    
    // MARK: - Private Properties
    
    private var googleMapsView: GMSMapView!
    private var clusterManager: GMUClusterManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        bindEvents()
        showSourceSelectionAlert()
    }
    
    
    // MARK: - Private Methods
    
    /// Настройка UI
    private func configureUI() {
        let camera = GMSCameraPosition.camera(withLatitude: 57.48688, longitude: 45.1390676, zoom: 4)
        self.googleMapsView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        self.googleMapsView.settings.myLocationButton = true
        self.googleMapsView.isMyLocationEnabled = true
        self.view = googleMapsView
        
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: googleMapsView,
                                                 clusterIconGenerator: iconGenerator)
        clusterManager = GMUClusterManager(map: googleMapsView, algorithm: algorithm,
                                           renderer: renderer)
    }
    
    /// Подписка на обновления Presentation Model
    private func bindEvents() {
        presentationModel.changeStateHandler = { [unowned self] status in
            switch status {
            case .loading:
                DispatchQueue.main.async {
                    MBProgressHUD.showAdded(to: self.view, animated: true)
                }
            case .error(message: let message):
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.showErrorAlert(title:Constants.errorAlertTitle,
                                        message: message)
                }
                print(message)
            case .rich:
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            }
        }
    }
    
    
    private func showSourceSelectionAlert() {
        
        let alert = UIAlertController(title: Constants.alertTitle,
                                      message: Constants.alertMessage,
                                      preferredStyle: .alert)
        
        let transportAction = UIAlertAction(title: Constants.sourceTypeTransport,
                                            style: .default) { (action) in
                                               self.obtainData(from: .transport)
        }
        let transportRMRAction = UIAlertAction(title: Constants.sourceTypeRMRTransport,
                                            style: .default) { (action) in
                                                self.obtainData(from: .transportRMR)
        }
        let transportJSONAction = UIAlertAction(title: Constants.sourceTypeJSON,
                                            style: .default) { (action) in
                                                self.obtainData(from: .jsonFile)
        }
        alert.addAction(transportAction)
        alert.addAction(transportRMRAction)
        alert.addAction(transportJSONAction)
    
        self.present(alert, animated: true, completion: nil)
    }
    
    private func obtainData(from source: SourceType) {
        presentationModel.obtainOffices(from: source) { [weak self] (coordinates) in
            for cluster in coordinates {
                self?.clusterManager.add(cluster)
            }
            DispatchQueue.main.async {
                self?.clusterManager.cluster()
            }
        }
    }
    
}
