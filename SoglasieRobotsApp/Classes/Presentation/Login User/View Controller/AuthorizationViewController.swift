//
//  AuthorizationViewController.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 29.01.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import UIKit
import MBProgressHUD
import InputMask
import RxSwift
import RxCocoa


final class AuthorizationViewController: BaseViewController {
    
    // MARK: - Constants
    
    private enum Constants {
        
        /// Localization
        static let title = NSLocalizedString("auth_title_label", comment: "")
        static let phonePlaceholder = NSLocalizedString("auth_phone_placeholder_text", comment: "")
        static let passwordPlaceholder = NSLocalizedString("auth_password_placeholder_text", comment: "")
        static let submitButton = NSLocalizedString("auth_submit_button_title", comment: "")
        static let mapButton = NSLocalizedString("auth_map_button_title", comment: "")
        static let officesTableButton = NSLocalizedString("auth_offices_table_button_title", comment: "")
        
        static let fieldsEmptyError = NSLocalizedString("auth_err_fields_empty", comment: "")
        static let alertTitle = NSLocalizedString("alert_ok_button", comment: "")
        
        /// Segue IDs
        static let showUserInfoSegueId = "ShowUserInfoSegue"
        static let showPasscodeSegue = "ShowPasscodeSegue"
        static let showMapSegueId = "ShowMapSegue"
        static let showOfficesTableSegueId = "ShowTableOfficesSegue"
        
        static let minimumPhoneLenght = 5
        static let minimumPassLenght = 3
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var phoneTextField: UITextField!
    
    @IBOutlet var passwordTextField: UITextField!
    
    @IBOutlet var submitButton: UIButton!
    
    @IBOutlet var mapButton: UIButton!
    
    @IBOutlet var officesTableButton: UIButton!
    
    
    // MARK: - Public Properties
    
    let presentationModel = AuthorizationPresentationModel()
    
    
    // MARK: - Private Properties
    private var maskedDelegate: NotifyingMaskedTextFieldDelegate!
    
    private let disposeBag = DisposeBag()
    
    // MARK: - ViewController lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        checkAuth()
        configureUI()
        bindEvents()
    }
    
    /// Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.showPasscodeSegue {
            guard let passcodeVc = segue.destination as? PasscodeViewController else {
                assertionFailure("Missplased destitation controller")
                return
            }
            if let userViewModel = sender as? UserViewModel {
                passcodeVc.passcodeViewType = .setPassword
                passcodeVc.userViewModel = userViewModel
            } else {
                passcodeVc.passcodeViewType = .checkPassword
            }
        }
    }
    
    // MARK: - IBActions
    
    @IBAction func mapButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: Constants.showMapSegueId, sender: nil)
    }
    
    
    @IBAction func officesTableButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: Constants.showOfficesTableSegueId, sender: nil)
    }
    
    
    @IBAction func submitButtonPressed(_ sender: Any) {
        guard let phone = self.phoneTextField.text,
            let pass = passwordTextField.text else {
                self.showErrorAlert(title: Constants.alertTitle,
                                    message: Constants.fieldsEmptyError)
                return
        }
        presentationModel.authenticateUser(phoneNumber: phone, password: pass) { [weak self] (userViewModel) in
            if let viewModel = userViewModel {
                self?.performSegue(withIdentifier: Constants.showPasscodeSegue, sender: viewModel)
            }
        }
    }
    
    // MARK: - Private Methods
    
    private func checkAuth() {
        if presentationModel.isUserAuthorized() {
            self.performSegue(withIdentifier: Constants.showPasscodeSegue, sender: nil)
        }
    }
    
    /// Настройка UI
    private func configureUI() {
        titleLabel.text = Constants.title
        phoneTextField.placeholder = Constants.phonePlaceholder
        passwordTextField.placeholder = Constants.passwordPlaceholder
        submitButton.setTitle(Constants.submitButton, for: .normal)
        mapButton.setTitle(Constants.mapButton, for: .normal)
        officesTableButton.setTitle(Constants.officesTableButton, for: .normal)
        
        titleLabel.tintColor = Colors.black
        phoneTextField.textColor = Colors.red
        submitButton.backgroundColor = Colors.orange
        submitButton.tintColor = Colors.green
        
        submitButton.isEnabled = false
        
        phoneTextField.autocorrectionType = .no
        passwordTextField.autocorrectionType = .no
        maskedDelegate = NotifyingMaskedTextFieldDelegate(format: "{+7} ([000]) [000] [00] [00]")
        phoneTextField.delegate = maskedDelegate
        passwordTextField.delegate = self
    }
    
    /// Подписка на обновления Presentation Model
    private func bindEvents() {
        presentationModel.changeStateHandler = { [unowned self] status in
            switch status {
            case .loading:
                MBProgressHUD.showAdded(to: self.view, animated: true)
            case .error(message: let message):
                self.passwordTextField.text = nil
                self.phoneTextField.text = nil
                MBProgressHUD.hide(for: self.view, animated: true)
                self.showErrorAlert(title: Constants.alertTitle,
                                    message: NSLocalizedString(message, comment: ""))
                print(message)
            case .rich:
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }


        let phoneValid = maskedDelegate.rx_textfield
            .map { $0.text!.count >= Constants.minimumPhoneLenght }
            .share(replay: 1)
        
        let passwordValid = passwordTextField.rx.text.orEmpty
            .map { $0.count >= Constants.minimumPassLenght }
            .share(replay: 1)
        
        let everythingValid = Observable
            .combineLatest(phoneValid, passwordValid) { $0 && $1 }
            .share(replay: 1)
        
        everythingValid
            .bind(to: submitButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
    }
}

extension AuthorizationViewController: UITextFieldDelegate {
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
    
}
