//
//  UserInfoViewController.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 30.01.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import UIKit

class UserInfoViewController: UIViewController {

    private enum Constants {
        static let unknownFirstNameText = NSLocalizedString("user_info_unknown_first_name", comment: "")
        static let unknownSecondName = NSLocalizedString("user_info_unknown_second_name", comment: "")
        static let policyButtonText = NSLocalizedString("user_info_policy_button_text", comment: "")
    }
    
    // MARK: - IBOutlets
    
    @IBOutlet var firstNameLabel: UILabel!
    
    @IBOutlet var secondNameLabel: UILabel!
    
    @IBOutlet var phoneLabel: UILabel!
    
    @IBOutlet var policyButton: UIButton!
    
    // MARK: - Public Properties
    
    let presentationModel = UserInfoPresentationModel()
    var userViewModel: UserViewModel?
    
    // MARK: - ViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        fulfillForms()
    }

    // MARK: - Private Methods
    
    /// Настройка UI
    private func configureUI() {
        firstNameLabel.textColor = Colors.green
        secondNameLabel.textColor = Colors.orange
        phoneLabel.textColor = Colors.red
        policyButton.setTitle(Constants.policyButtonText, for: .normal)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Выход",
                                                                style: .plain,
                                                                target: self,
                                                                action: #selector(exitButtonPressed(_:)))
    }
    
    @objc private func exitButtonPressed(_ sender: Any) {
        self.presentationModel.clearAuthData()
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    private func fulfillForms() {
        phoneLabel.text = userViewModel?.phone
        if let firstName = userViewModel?.firstName {
            self.firstNameLabel.text = firstName
        } else {
            self.firstNameLabel.text = Constants.unknownFirstNameText
        }
        if let secondName = userViewModel?.secondName {
            self.secondNameLabel.text = secondName
        } else {
            self.secondNameLabel.text = Constants.unknownSecondName
        }
    }
}
