//
//  RxRMRTextFieldDelegateProxy.swift
//  SoglasieRobotsApp
//
//  Created by Alex on 13.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

extension NotifyingMaskedTextFieldDelegate {
    
    var rx_editing_listenner: DelegateProxy<NotifyingMaskedTextFieldDelegate, NotifyingMaskedTextFieldDelegateListener> {
        return RxRMRTextFieldDelegateProxy.proxy(for: self)
    }
    
    var rx_textfield: Observable<UITextField> {
        let proxy = RxRMRTextFieldDelegateProxy.proxy(for: self)
        return proxy.textFieldSubject
    }
}

final class RxRMRTextFieldDelegateProxy: DelegateProxy<NotifyingMaskedTextFieldDelegate, NotifyingMaskedTextFieldDelegateListener>, DelegateProxyType, NotifyingMaskedTextFieldDelegateListener {
    
    func onEditingChanged(inTextField: UITextField) {
        textFieldSubject.onNext(inTextField)
    }
    
    let textFieldSubject = PublishSubject<UITextField>()
    private weak var control: NotifyingMaskedTextFieldDelegate? = nil
    
    init(parentObject: NotifyingMaskedTextFieldDelegate) {
        self.control = parentObject
        super.init(parentObject: parentObject, delegateProxy: RxRMRTextFieldDelegateProxy.self)
    }
    
    static func registerKnownImplementations() {
        self.register { RxRMRTextFieldDelegateProxy(parentObject: $0) }
    }
    
    static func currentDelegate(for object: NotifyingMaskedTextFieldDelegate) -> NotifyingMaskedTextFieldDelegateListener? {
        return object.editingListener
    }
    
    static func setCurrentDelegate(_ delegate: NotifyingMaskedTextFieldDelegateListener?, to object: NotifyingMaskedTextFieldDelegate) {
        object.editingListener = delegate
    }
    
    
}
