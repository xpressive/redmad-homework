//
//  SourceType.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 07.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

/// Источник загрузки данных
enum SourceType {
    case transport
    case transportRMR
    case jsonFile
}
