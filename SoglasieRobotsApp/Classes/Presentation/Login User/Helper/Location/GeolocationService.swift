//
//  GeolocationService.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 14.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import CoreLocation
import RxSwift
import RxCocoa

final class GeolocationService {
    
    private (set) var authorized: Driver<Bool>
    private (set) var location: Driver<CLLocation>
    
    private let locationManager = CLLocationManager()
    
    init() {
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        
        authorized = Observable.deferred { [weak locationManager] in
            let status = CLLocationManager.authorizationStatus()
            guard let locationManager = locationManager else {
                return Observable.just(status)
            }
            return locationManager
                .rx.didChangeAuthorizationStatus
                .startWith(status)
            }
            .asDriver(onErrorJustReturn: CLAuthorizationStatus.notDetermined)
            .map {
                switch $0 {
                case .authorizedAlways:
                    return true
                default:
                    return false
                }
        }
        
        location = locationManager.rx.didUpdateLocations
            .asDriver(onErrorJustReturn: [])
            .flatMap {
                return $0.last.map(Driver.just) ?? Driver.empty()
        }
        
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func stopUpdating() {
        self.locationManager.stopUpdatingLocation()
    }
    
}
