//
//  User.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 29.01.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

struct User: Codable {
    
    let phone: String
    
    let isPhoneVerified: Bool
    
    let id: Int
    
    let firstName: String?
    
    let secondName: String?
    
    let middleName: String?
    
    let birthday: Double?
    
    let email: String?
    
    let isEmailVerified: Bool
    
    private enum CodingKeys: String, CodingKey {
        case phone = "phone"
        case isPhoneVerified = "is_phone_verified"
        case id = "id"
        case firstName = "first_name"
        case secondName = "second_name"
        case middleName = "middle_name"
        case birthday = "birthday"
        case email = "email"
        case isEmailVerified = "is_email_verified"
    }
    
}
