//
//  Office.swift
//  SoglasieRobotsApp
//
//  Created by Alexey Kuznetsov on 06.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

struct Office: Codable {
    
    let city: String
    
    let address: String
    
    let coordinate: Coordinate
    
}

struct Coordinate: Codable {
    
    let lat: Double
    let lng: Double
    
    private enum CodingKeys: String, CodingKey {
        case lat = "latitude"
        case lng = "longitude"
    }
    
}
