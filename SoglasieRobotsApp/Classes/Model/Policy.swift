//
//  Policy.swift
//  SoglasieRobotsApp
//
//  Created by Alex on 13.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

enum PoliceType: String, Codable {
    case osago
    case kasko
    case eosago
    case dms
    case property
    case vzr
}

struct Policy: Codable {
    
    let id: Int
    
    let title: String
    
    let number: String
    
    let type: PoliceType
    
}
