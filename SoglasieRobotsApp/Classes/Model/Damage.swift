//
//  Damage.swift
//  SoglasieRobotsApp
//
//  Created by Alex on 13.02.2018.
//  Copyright © 2018 Alexey Kuznetsov. All rights reserved.
//

import Foundation

struct Damage: Codable {
    
    let id: Int
    
    let policyTitle: String
    
    let policyType: PoliceType
    
    let policyNumber: String
    
    let number: String
    
    let eventDate: Double
    
    private enum CodingKeys: String, CodingKey {
        case id
        case policyTitle = "policy_title"
        case policyType = "policy_type"
        case policyNumber = "policy_number"
        case number
        case eventDate = "event_date"
    }
    
}
